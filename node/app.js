const T = require('./twitter')
const { Client } = require('@elastic/elasticsearch')
const Sentiment = require('sentiment')
const sentiment = new Sentiment()

const indice = process.env.indice || 'trump'

async function run() {
  const client = new Client({
    node: 'http://elastic:changeme@elasticsearch:9200',
    log: 'trace'
  })

  let tweets = []

  const addElem = (elem) => {
    tweets.push(elem)

    if (tweets.length > 10) {
      const body = tweets.flatMap(doc => [{ index: { _index: indice.replace(/\s/g, '_').toLowerCase() } }, doc])
      client.bulk({
        refresh: true, body
      })
        .then(({ body: bulkResponse }) => {
          if (bulkResponse.errors) {
            const erroredDocuments = []
            // The items array has the same order of the dataset we just indexed.
            // The presence of the `error` key indicates that the operation
            // that we did for the document has failed.
            bulkResponse.items.forEach((action, i) => {
              const operation = Object.keys(action)[0]
              if (action[operation].error) {
                erroredDocuments.push({
                  // If the status is 429 it means that you can retry the document,
                  // otherwise it's very likely a mapping error, and you should
                  // fix the document before to try it again.
                  status: action[operation].status,
                  error: action[operation].error,
                  operation: body[i * 2],
                  document: body[i * 2 + 1]
                })
              }
            })
            console.log(erroredDocuments[0])
          } else {
            console.log('Ok')
          }
        })
      tweets = []
    }
  }

  await client.indices.create({
    index: indice,
    body: {
      mappings: {
        properties: {
          text: {
            type: 'text',
            analyzer: "simple"
          },
          user: { type: 'text' },
          lang: { type: 'text' },
          time: { type: 'date' },
          sentiment: { type: 'keywords' }
        }
      }
    }
  }, { ignore: [400] })

  const twitterStream = T.stream('statuses/filter', { track: indice })

  twitterStream.on('data', data => {
    if (!data.text) return
    let tweetSentiment = sentiment.analyze(data.text).score
    console.log({ tweetSentiment })

    if (tweetSentiment === 0) tweetSentiment = 'neutral'
    else tweetSentiment = tweetSentiment > 0 ? 'positive' : 'negative'


    const tweet = {
      text: data.text,
      user: data.user?.screen_name,
      time: (data.created_at && new Date(data.created_at)) || new Date(),
      lang: data.lang,
      sentiment: tweetSentiment
    }

    addElem(tweet)
  })

  twitterStream.on('error', error => console.log(error))
}


const app = () => {
  try {
    setTimeout(run, 30000)
  } catch (error) {
    console.log(error)
    app()
  }
}

app()
