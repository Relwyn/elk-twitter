const { Client } = require('@elastic/elasticsearch')

const client = new Client({
  node: `http://elastic:changeme@elasticsearch:9200`,
  log: 'trace'
})

const bulkInsert = datasource => client.helpers.bulk({
  datasource,
  onDocument(doc) {
    console.log({ doc })
    return {
      index: { _index: 'tweets-macron' }
    }
  }
})

module.exports = bulkInsert